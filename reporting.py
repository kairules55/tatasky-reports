"""
Script to export files from on-prem server
"""

import io
import os
from datetime import date, datetime, timedelta, timezone, time
# from typing_extensions import final
import pandas as pd
# from typing_extensions import final
from fsplit.filesplit import Filesplit
from collections import Counter
import numpy as np
import boto3
import sys
import base64
import requests
import seaborn as sns
import json
import redis
import time
from pathlib import Path
import pygsheets
import smtplib
from botocore.exceptions import NoCredentialsError
from prettytable import PrettyTable
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders 
from google.oauth2 import service_account
from googleapiclient.discovery import build
from googleapiclient.http import MediaIoBaseDownload
import smtplib 
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders 
import plotly.graph_objects as go 

fs = Filesplit()

"""
export SMTP_USER=user@email.com
export SMTP_PASS=password
export EMAIL_SENDER=sender@skit.ai
export EMAIL_RECEIVER=receiver@skit.ai
docker run \
    --env SMTP_USER=user@gmail.com \
    --env SMTP_PASS=password \
    --env EMAIL_SENDER=sender@skit.ai  \
    --env EMAIL_RECEIVER=receiver@skit.ai \
    --env SMTP_HOST=smtp.gmail.com \
    --env SMTP_PORT=587 \
    --env USE_TLS=false \
    --env USE_LOGIN=false \
    --env METABASE_API_ENDPOINT=https://metabase.vernacular.ai/api \
    --env METABASE_USER=admin@skit.ai \
    --env METABASE_PASS=password \
    --env REDIS_REMOTE_HOST=host.docker.internal \
    --env REDIS_REMOTE_PORT=6379 \
    --env REDIS_DB=0 \
    --env METABASE_REFRESH_KEY_DURATION=1036800 \
    --env QUESTION_ID=1063 \
    --env FILE__SIZE=3000000 \
debugger:latest python3 scripts/file_exporter.py
"""

#############################################################################################################
# Environment Variables                                                                                     #
#############################################################################################################
METABASE_API_ENDPOINT = os.environ.get('METABASE_API_ENDPOINT', 'https://metabase.skit.ai/api')
METABASE_USER = os.environ.get('METABASE_USER','automation@skit.ai')
METABASE_PASS = os.environ.get('METABASE_PASS','v8rvern@cul@r')

REDIS_REMOTE_HOST = os.environ.get('REDIS_REMOTE_HOST', 'localhost')
REDIS_REMOTE_PORT = os.environ.get('REDIS_REMOTE_PORT', '6379')
REDIS_DB = os.environ.get('REDIS_DB', '0')

FILE_NAME = os.environ.get('FILE_NAME', 'file')
SAVE_EXPORT_TO_LOCAL = os.environ.get('SAVE_EXPORT_TO_LOCAL', 'false')
USE_DATE_FOR_FILE_NAME = os.environ.get('USE_DATE_FOR_FILE_NAME', 'true')

METABASE_REFRESH_KEY_DURATION = os.environ.get('METABASE_REFRESH_KEY_DURATION', '1036800') # Default: 12 days

QUESTION_ID_TRAVERSAL = int(os.environ.get('QUESTION_ID_TRAVERSAL', '1400')) #1447 #1400
QUESTION_ID = int(os.environ.get('QUESTION_ID', '1390'))
MAIL_TRIGGER = os.environ.get('MAIL_TRIGGER', 'FALSE')
REPORT_TYPE = os.environ.get('REPORT_TYPE', 'Daily')
REPORT_LANGUAGE = os.environ.get('REPORT_LANGUAGE', 'Overall')
FILE__SIZE = int(os.environ.get('FILE__SIZE', '1000000'))
DRIVE_FOLDER_ID = os.environ.get('DRIVE_FOLDER_ID','1KpH0ifO8ihIxOT6CTBYUSuenfQRngpzK')
SHEET_ID = os.environ.get('SHEET_ID','18YEQ-autm0b-ro-kJ_XMHuB4r13bCeqLBl4sEkig3vo')
#############################################################################################################


#############################################################################################################
#                                      STATE MAP                                                            #
#############################################################################################################
state_map = {
    # 'COF_WORK_ORDER': 'Opening State',
    'NOT_SCHEDULED_AFTER_2_HOURS': 'Workorder Status',
    'NOT_SCHEDULED_NON_OCS': 'Workorder Status',
    'NOT_SCHEDULED_WORK_ORDER_OCS': 'Workorder Status',
    'SCHEDULED_WORK_ORDER_NON_OCS': 'Workorder Status',
    'TIME_LAPSED_NON_OCS': 'Workorder Status',
    'SCHEDULED_WORK_ORDER_IN_PROGRESS_NON_OCS': 'Workorder Status',
    'SCHEDULED_WORK_ORDER_OCS': 'Workorder Status',
    'TIME_LAPSED_OCS': 'Workorder Status',
    'SCHEDULED_WORK_ORDER_IN_PROGRESS_OCS': 'Workorder Status',    
    # 'TRANSFER_AGENT_INVALID_USECASE_VALUE': 'Transfer Agent Invalid',
    # 'TRANSFER_AGENT_INVALID_REGION_VALUE': 'Transfer Agent Invalid',
    # 'TRANSFER_AGENT_INVALID_WORKORDER_VALUES': 'Transfer Agent Multiple Workorders',
    # 'TRANSFER_AGENT_MULTIPLE_WORK_ORDER': 'Transfer Agent Multiple Workorders',
    # 'SUBSEQUENT_QUERY_COF_STATUS': 'Subsequent Query',
    # 'SUBSEQUENT_QUERY_COF': 'Subsequent Query',
    # 'HANGUP_SUCCESSFUL': 'Hangup Successful',
    # 'HANGUP_SUCCESSFUL_STATUS': 'Hangup Successful',
    # 'TRANSFER_AGENT': 'Transfer Agent',
    # 'TRANSFER_AGENT_OOS': 'Transfer Agent',
    # 'TRANSFER_IVR_SILENCE': 'Transfer Agent',
    # 'TRANSFER_IVR_DROP_CHANNEL_MENU': 'Transfer IVR',
    # 'TRANSFER_IVR_ADD_CHANNEL_MENU': 'Transfer IVR',
    # 'TRANSFER_IVR': 'Transfer IVR',
    # 'TRANSFER_IVR_BALANCE_MENU': 'Transfer IVR',
    # 'TRANSFER_IVR': 'Transfer IVR',
    # 'TRANSFER_IVR_RMN_MENU': 'Transfer IVR',
    # 'TRANSFER_IVR_LANGUAGE_MENU': 'Transfer IVR',
    # 'TRANSFER_IVR_STATE_MAIN_MENU': 'Transfer IVR',
    # 'TRANSFER_IVR_PACK_MENU': 'Transfer IVR',
    # 'TRANSFER_IVR_UNABLE_TO_VIEW_MENU': 'Transfer IVR',
    # 'TRANFER_AGENT_SILENCE' : 'Transfer Agent',
}  


REDIS_METABASE_API_TOKEN_KEY = 'file_exporter:metabase_api:token'
REDIS_METABASE_API_TOKEN_UPDATED_AT_KEY = 'file_exporter:metabase_api:token:updated_at'

GOOGLE_APPLICATION_CREDENTIALS = os.environ.get(
    'GOOGLE_APPLICATION_CREDENTIALS',
    'service-account-creds.json')
gc = pygsheets.authorize(service_account_file=GOOGLE_APPLICATION_CREDENTIALS)

API_TOKEN = None
NOW = datetime.now().strftime("%d-%m-%Y")

if USE_DATE_FOR_FILE_NAME == 'true':
    FILE_NAME = f"{NOW}-{FILE_NAME}"

SHEET_FOLDER = 'reports/report_files'
os.makedirs(SHEET_FOLDER, exist_ok=True)

IMAGE_FOLDER = 'reports/images'
os.makedirs(IMAGE_FOLDER, exist_ok=True)

CSV_FOLDER = 'reports/csv_data'
os.makedirs(CSV_FOLDER, exist_ok=True)


AWS_ACCESS_KEY_ID = 'AKIAXZ4E7LFC6JIXPLHB'
AWS_SECRET_ACCESS_KEY = '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'


def create_aws_folder(time_now):
    folder = f"out/reports/{time_now.year}/{time_now.strftime('%m')}/{time_now.strftime('%d')}"
    os.makedirs(folder,exist_ok=True)
    print(folder.format())
    return folder.format()

def percent_cb(complete, total):
        sys.stdout.write('.')
        sys.stdout.flush()


def upload_to_aws(local_file, bucket, s3_file):
    s3 = boto3.client('s3', aws_access_key_id=AWS_ACCESS_KEY_ID,
                      aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

    try:
        s3.upload_file(local_file, bucket, s3_file)
        print("Upload Successful")
        return True
    except FileNotFoundError:
        print("The file was not found")
        return False
    except NoCredentialsError:
        print("Credentials not available")
        return False
    

    

def create_service(service_name, service_version):
    SCOPES = [
        'https://www.googleapis.com/auth/presentations',
        'https://www.googleapis.com/auth/spreadsheets',
        'https://www.googleapis.com/auth/drive']
    creds = service_account.Credentials.from_service_account_file(
        GOOGLE_APPLICATION_CREDENTIALS, scopes=SCOPES)

    service = build(service_name, service_version, credentials=creds)
    return service


def copy_to_folder(fileId, target_name):
    drive_service = create_service('drive', 'v3')

    request_body = {
        'name': target_name,
    }
 
    request_body['parents'] = [DRIVE_FOLDER_ID]

    res = drive_service.files().copy(fileId=SHEET_ID, body=request_body).execute()

    copied_file_id = res.get('id')
    return copied_file_id


def download_file(fileId, filename):
    drive_service = create_service('drive', 'v3')

    mimetype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    request = drive_service.files().export_media(fileId=fileId,
                                                 mimeType=mimetype)
    fh = io.FileIO(filename, 'wb')
    downloader = MediaIoBaseDownload(fh, request)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
    fh.close()
    return filename


def update_sheet(sheet_name,values,crange):
    sh = gc.open_by_key(SHEET_ID)
    worksheet = sh.worksheet('title',sheet_name)
    worksheet.clear()
    worksheet.update_values(crange=crange, values=values)

# def update_traversal_sheet(sheet_data):
#     sh = gc.open_by_key(SHEET_ID)
#     worksheet = sh.worksheet('title','Traversal')
#     worksheet.clear()
#     worksheet.update_values(crange='A:E', values=sheet_data)

def get_api_token():
    redis_available = True
    try:
        redis_conn = redis.Redis(host=REDIS_REMOTE_HOST, port=REDIS_REMOTE_PORT, db=REDIS_DB)
        updated_at = redis_conn.get(REDIS_METABASE_API_TOKEN_UPDATED_AT_KEY)
        if updated_at and (time.time() - float(updated_at)) < int(METABASE_REFRESH_KEY_DURATION):
            token = redis_conn.get(REDIS_METABASE_API_TOKEN_KEY)
            if token:
                return token.decode('ascii')
    except:
        redis_available = False

    url = f"{METABASE_API_ENDPOINT}/session"
    headers = {'Content-Type': 'application/json'}
    data = {
        'username': METABASE_USER,
        'password': METABASE_PASS
    }

    response = requests.post(url, data=json.dumps(data), headers=headers)
    response = response.json()
    token = response.get('id')

    if redis_available:
        redis_conn.set(REDIS_METABASE_API_TOKEN_KEY, token)
        redis_conn.set(REDIS_METABASE_API_TOKEN_UPDATED_AT_KEY, str(time.time()))

    return token

def get_export_file(question_id):
    url = f"{METABASE_API_ENDPOINT}/card/{question_id}/query/csv"
    headers = {'X-Metabase-Session': API_TOKEN}
    response = requests.post(url, headers=headers)
    return response.content

def get_state_name(state):
    if state in state_map:
        return state_map[state]
    return state

x_cordinates = []
y_cordinates = []

def coordinate(labels):
    for label in labels:
        turn  = (int(label.split(': ')[1][5:])-1)*0.22
        x_cordinates.append(turn)
        y_cordinates.append(0.1)

def grenerate_report(content):
    original = io.BytesIO(content)
    df = pd.read_csv(original)
    print(df)
    df_group = df.groupby('call_id')
    transitions = list()
    current_global_id = 1

    id_map = {
        "0: Turn 0: Start": 1,
    }
    count_map = {
        1: "All"
    }
    for call, conversations in df_group:
        previous_state = "Start"
        current_id = 0
        previous_level = 0
        for i, c in conversations.iterrows():
            if previous_state == get_state_name(c["state"]):
                continue
            previous_level += 1
                
            parent_name = f"{current_id}: Turn {previous_level - 1}: {previous_state}"
            if not parent_name in id_map:
                current_global_id += 1
                id_map[parent_name] = current_global_id
                
            child_name = f"{id_map[parent_name]}: Turn {previous_level}: {get_state_name(c['state'])}"
            if not child_name in id_map:
                current_global_id += 1
                id_map[child_name] = current_global_id
            
            transitions.append(f"{parent_name},{child_name}")
            previous_state = get_state_name(c["state"])
            current_id = id_map[parent_name]
    
    counter = dict(Counter(transitions))
    final_values = list()
    for k, count in counter.items():
        parent,child = k.split(",")
        count_map[id_map[child]] = count
        
    for k, count in counter.items():
        parent,child = k.split(",")
        final_values.append({
            "parent": f"{parent} ({count_map[id_map[parent]]})",
            "child": f"{child} ({count_map[id_map[child]]})"
        })

    relation_map = {}
    for k, count in counter.items():
        parent,child =k.split(",")
        if parent not in relation_map:
            relation_map[parent] = count_map[id_map[child]]
        else:
            relation_map[parent] += count_map[id_map[child]]

    for k,v in relation_map.items():
        id = k.split(':')[0]
        turn = k.split(' ')[2]
        turn = turn.split(':')[0]
        turn = int(turn)+1
        if (isinstance(count_map[id_map[k]],str)):
            continue
        disconnected = count_map[id_map[k]]-v
        final_values.append({
            "parent": f"{k} ({count_map[id_map[k]]})",
            "child": f"{id}: Turn {turn}: Disconnected ({disconnected})"
        })
    
    sankey = {}
    source = []
    target = []
    values =  [] 
    labels = []
    index = 0
    bad_chars = ['(',')']
    for f_value in final_values:
        if 'All' in f_value['parent']:
            continue
        if f_value['parent'] not in sankey:
            sankey[f_value['parent']] = index
            labels.append(f_value['parent'])
            index += 1
        if f_value['child'] not in sankey:
            sankey[f_value['child']] = index
            labels.append(f_value['child'])
            index += 1
        source.append(sankey[f_value['parent']])
        target.append(sankey[f_value['child']])
        value_array = f_value['child'].split(" ")
        test_string = value_array[-1]
        test_string = ''.join((filter(lambda i: i not in bad_chars, test_string)))
        values.append(int(test_string))

    coordinate(labels)

    fig = go.Figure(data=[go.Sankey(
        arrangement='snap',
        node = dict(
        pad = 15,
        thickness = 20,
        label = [x.split(': ')[2] for x in labels],
        line = dict(color = "black", width = 0.5),
        x=x_cordinates,
        y=y_cordinates
        ),
        link = dict(
        source = source,
        target = target,
        value = values
    ))])

    fig.update_layout(title_text="Tatasky Waterfall", font_size=10)
    fig.show()
    #waterfall_data = waterfall_report(labels)
    waterfall_data = waterfall_report_updated(final_values)
    # sheet = pd.DataFrame(f"{x.(': ')[1]}: {x.(': ')[2]}" for x in labels)
    # sheet = pd.DataFrame(final_values)
    # return sheet,fig
    waterfall_df = pd.DataFrame(dict([ (k,pd.Series(v)) for k,v in waterfall_data.items() ]))
    waterfall_df.fillna("",inplace=True)
    return waterfall_df,fig


def waterfall_report_updated(values):
    waterfall_map = {}
    countmap = {}
    for value in values:
        parent = value['parent'].split(': ')
        child = value['child'].split(': ')
        parent_turn = parent[1]
        if parent_turn != 'Turn 0':
            parent_value = parent[-1].split(' ')[0]
            parent_count = int(parent[-1].split(' ')[-1][1:-1])
            child_turn = child[1]
            child_value = child[-1].split(' ')[0]
            child_count = int(child[-1].split(' ')[-1][1:-1])
            final_value = f"{parent_turn}: {parent_value}-{parent_count} -> {child_turn}: {child_value}-{child_count}"
            if parent_turn not in waterfall_map:
                waterfall_map[parent_turn] = [final_value]
            else:
                waterfall_map[parent_turn].append(final_value)
            if parent_turn not in countmap:
                countmap[parent_turn] = child_count
            else:
                countmap[parent_turn] += child_count
            
    for key in countmap:
        waterfall_map[key].insert(0,f"Total Count : {countmap[key]}")
    return waterfall_map

def waterfall_report(labels):
    turnmap = {}
    countmap = {}
    for l in labels:
        label = l.split(': ')
        turn  = label[1]
        count = int(label[-1].split(' ')[-1][1:-1])
        key = f"{label[1]}: {label[-1].split(' ')[0]} - {label[-1].split(' ')[-1][1:-1]}"
        if turn not in turnmap:
            turnmap[turn] = [key]
        else:
            turnmap[turn].append(key) 
        if turn not in countmap:
            countmap[turn] = count
        else:
            countmap[turn] += count
    
    for key in countmap:
        turnmap[key].insert(0,f"Total Count : {countmap[key]}")
    return turnmap

def save_fig(fig,name):
    if not os.path.exists("images"):
        os.mkdir("images")
    path = f"{IMAGE_FOLDER}/{name}.png"
    fig.write_image(path,width=1920,height=1080)


def send_mail(filenames,time, uc3_breakdown, uc3_contained):
    fromaddr = "krishana@skit.ai"
    # recipients = ['srihitha@skit.ai','Tabrez.Anwar@tatasky.com','indumati.regunathan@tatasky.com','sudhindrav@tatasky.com','nazara@tatasky.com','uatteam@tatasky.com','mukunth@skit.ai','sreetama@skit.ai','karthik@skit.ai','anbud@smartconnectt.com','ashfaquem@smartconnectt.com,','vivekananda.prasad@tatasky.com','manikeshwaric@smartconnectt.com','pravj@skit.ai','divyansh.tiwari@skit.ai']
    # recipients = ['krishana@skit.ai','srihitha@skit.ai']
    recipients = ['krishana@skit.ai']
    if MAIL_TRIGGER == "TRUE":
        recipients = ['srihitha@skit.ai','Tabrez.Anwar@tatasky.com','indumati.regunathan@tatasky.com','sudhindrav@tatasky.com','nazara@tatasky.com','uatteam@tatasky.com','mukunth@skit.ai','sreetama@skit.ai','karthik@skit.ai','anbud@smartconnectt.com','ashfaquem@smartconnectt.com,','vivekananda.prasad@tatasky.com','manikeshwaric@smartconnectt.com','pravj@skit.ai','divyansh.tiwari@skit.ai']

    suffix = f"{time.hour} AM"
    if time.hour >= 12:
        suffix = f"{time.hour} PM"

    if REPORT_TYPE=='Daily': 
        if MAIL_TRIGGER == "TRUE":
            recipients = ['krishana@skit.ai','srihitha@skit.ai','Tabrez.Anwar@tatasky.com','indumati.regunathan@tatasky.com','sudhindrav@tatasky.com','nazara@tatasky.com','uatteam@tatasky.com','prateek@skit.ai','gupta.prateek@skit.ai','mithun@skit.ai','mukunth@skit.ai','sreetama@skit.ai','karthik@skit.ai','anbud@smartconnectt.com','ashfaquem@smartconnectt.com,','vivekananda.prasad@tatasky.com','manikeshwaric@smartconnectt.com','lenins@tatasky.com','pravj@skit.ai','Jayaram.Karthik@tatasky.com','divyansh.tiwari@skit.ai','Akash.NagMB@tatasky.com']
        suffix = f"{time.strftime('%b')} {time.strftime('%d')}"

    msg = MIMEMultipart() 
    msg['From'] = fromaddr 
    msg['To'] = ", ".join(recipients)
    msg['Subject'] = f"Voicebot : {REPORT_TYPE} Report {suffix}" 
    
    # body = f"Dear Team, \n\n{REPORT_TYPE} {REPORT_LANGUAGE} Report {suffix}"
    # body += f"\n{uc3_breakdown}\n\nThanks & Regards"
    # body = f"Daily Report"
    body = uc3_breakdown.get_html_string()
    contained = uc3_contained.get_html_string()
    # msg.attach(MIMEText(body, 'plain')) 

    html = """\
    <html>
        <head>
        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 5px;
                text-align: left;    
            }    
        </style>
        </head>
    <body>
    <p>Dear Team, <br>
       Usecase 3 Summary
       %s <br><br>
       %s
    </p>
    </body>
    </html>
    """ % (body, contained)

    msg.attach(MIMEText(html, 'html'))

    for name in filenames:
        filename = name[0]
        attachment = open(os.path.join(name[1], name[0]), "rb") 
        p = MIMEBase('application', 'octet-stream') 
        p.set_payload((attachment).read()) 
        encoders.encode_base64(p) 
        p.add_header('Content-Disposition', "attachment; filename= %s" % filename) 
        msg.attach(p)
    
    s = smtplib.SMTP('smtp.gmail.com', 587) 
    s.starttls() 
    s.login(fromaddr, "dozljweutfieclsu") 
    text = msg.as_string() 
    s.sendmail(fromaddr, recipients, text) 
    s.quit() 

def get_time_and_name():
    _IST_OFFSET = timedelta(hours=5, minutes=30)
    _IST = timezone(_IST_OFFSET)
    time_now = datetime.now(_IST)
    if REPORT_TYPE=='Daily':
        time_now = time_now - timedelta(days=1)
    # name = f"{REPORT_LANGUAGE} {time_now}"
    name = f"Overall_{time_now.year}{time_now.strftime('%m')}{time_now.strftime('%d')}"
    return time_now,name

def traversal_report(content):
    original = io.BytesIO(content)
    df = pd.read_csv(original)
    df['created date'] = pd.to_datetime(df['created_at']).dt.strftime('%Y-%m-%d')
    df['created time'] = pd.to_datetime(df['created_at']).dt.strftime("%H:%M:%S")
    df["start time"] = ""
    df["end time"] = ""
    df["cronological order"] = ""
    df["serviced status"] = ""
    df["old status"] = ""
    df["status"].fillna("Disconnected by user",inplace=True)
    df["old status"].fillna("Disconnected by user",inplace=True)
    # df["status_1"] = df["status"]
    df_group = df.groupby(['call_id'])
    for call,conversation in df_group: 
        count = 0
        index = 0
        previous_state = ""
        status_map = []
        status_read = False
        for i,c in conversation.iterrows():
            #Cronoglogical Change
            df.at[i,"cronological order"] = count
            

            #Hindi English Abberivation
            if df.at[i,"language"] == "en":
                df.at[i,"language"] = "english"
            if df.at[i,"language"] == "hi":
                df.at[i,"language"] = "hindi"


            #Calculate Start Time and End time
            if count==0:
                time  = pd.Timedelta(c['created time'])
                total_time = time.seconds + c['duration']
                final_time = str(timedelta(seconds=total_time)).split('.')[0]
                # df.at[i,"end time"] = final_time
                df.at[i,"start time"] = f'{c["created date"]} {c["created time"]}'
                df.at[i,"end time"] = f"{c['created date']} {final_time}"
                previous_state = c["state"]
                count += 1
                index = i
                status_map.append(i)
                continue


            # Ignore duplicate States
            current_state = c["state"]
            if current_state == previous_state:
                df.drop(i, inplace=True)
                continue

            previous_state = current_state
            count += 1

            #Status Bifercate
            status_map.append(i)
            if df.at[i,"state"] in state_map:
                status_read = True

            #No blank cells in Raw file Change
            df.at[i,"start time"] = df.at[index,"start time"]
            df.at[i,"end time"] = df.at[index,"end time"]
        
        ind = status_map[-1]
        final_state = df.at[ind,"state"]
        final_status = ""

        services_status = "No"
        serviced_states = ['NOT_SCHEDULED_AFTER_2_HOURS','NOT_SCHEDULED_NON_OCS','NOT_SCHEDULED_WORK_ORDER_OCS','SCHEDULED_WORK_ORDER_NON_OCS','TIME_LAPSED_NON_OCS','SCHEDULED_WORK_ORDER_IN_PROGRESS_NON_OCS','SCHEDULED_WORK_ORDER_OCS','TIME_LAPSED_OCS','SCHEDULED_WORK_ORDER_IN_PROGRESS_OCS']
        if final_state in serviced_states or "HANGUP" in final_state:
            services_status = "Yes"

        if "IVR" in final_state:
            final_status = "Transferred to IVR-WO Read out" if status_read else "Transferred to IVR-Others"
        elif "AGENT" in final_state:
            final_status = "Transferred to Agent-WO Read out" if status_read else "Transferred to Agent-Others"
        elif "HANGUP" in final_state:
            final_status = "Serviced at Bot-WO Read out" if status_read else "Serviced at BOT-Others"
        else:
            final_status = "Disconnected by Customer-WO Read out" if status_read else "Disconnected by Customer-Others"

        for status in status_map:
            df.at[status,"status"] = final_status
            df.at[status,"old status"] = final_status
            df.at[status,"serviced status"] = services_status

    df["subscriber_id"].fillna("",inplace=True)
    df["uuid"].fillna("",inplace=True)
    df["caller_number"].fillna("",inplace=True)
    df["duration"].fillna("",inplace=True)
    df["status"].fillna("",inplace=True)
    df["old status"].fillna("",inplace=True)
    df["created time"].fillna("",inplace=True)
    df["created date"].fillna("",inplace=True)
    df["serviced status"].fillna("",inplace=True)
    df.replace('USER_HANGUP',"Disconnected by user")
    new_row = pd.DataFrame({'uuid':'uuid', 'subscriber_id':'subscriber_id', 'caller_number':'caller_number',
        'duration':'duration', 'state':'state', 'cronological order':'cronological order', 'serviced status':'serviced status', 'old status':'old status', 'start time':'start time', 'end time':'end time', 'language':'language', 'laststate':'laststate', 'usecase':'usecase', 'status':'status', 'remote_status':'remote_status'},
                                                                index =[0])
    dropped = df.drop(['call_id','created_at','created date','created time'], axis = 1)
    dropped = pd.concat([new_row, dropped]).reset_index(drop = True)
    # unique = dropped.drop_duplicates()
    return dropped


def extract_call_distribution(dataframe,uuid_collection):
    hangup_collection = dataframe.loc[(dataframe.status == 'HANGUP') & (dataframe.uuid.isin(uuid_collection))].uuid.unique()
    ivr_collection = dataframe.loc[(dataframe.state.str.contains('IVR')) & (dataframe.uuid.isin(uuid_collection))].uuid.unique()
    agent_collection = dataframe.loc[(dataframe.state.str.contains('AGENT')) & (dataframe.uuid.isin(uuid_collection))].uuid.unique()
    return len(hangup_collection), len(ivr_collection), len(agent_collection)


def extract_collection(dataframe,state):
    return dataframe.loc[dataframe.state.str.contains(state)].uuid.unique()


def print_distribution(dataframe,collection,collection_name):
    hangup_count, ivr_count, agent_count, last_state_count = extract_call_distribution(dataframe,collection)
    total_calls = len(collection)
    disconnect = total_calls - (hangup_count+agent_count+ivr_count)
    message = '''\
        {collection_name}: 
            Total Calls: {total_calls}
            AGENT Breakdown : {agent_count}
            IVR Breakdown : {ivr_count}
            Disconnect : {disconnect}
            Last State Count : {last_state_count}
            Hangup Breakdown : {hangup_count}.\
        '''.format(last_state_count=last_state_count,total_calls=total_calls,disconnect=disconnect,collection_name=collection_name,agent_count=agent_count,ivr_count=ivr_count,hangup_count=hangup_count)
    print(message)


def get_states(dataframe):
    states = ['CHANGE_BATTERIES', 'SUBSEQUENT_QUERY_COF_REMOTE_NO', 'CONFIRM_REPLACEMENT', 'PICK_TIME_SLOT_REMOTE']    
    status = ['UC3_YES', 'UC3_NO', 'UC3_PRICE_PROMPT', 'UC3_SLOT_PRESENTED']
    return [list(collection) for collection in zip(states,status)]
    

def create_collections(dataframe):
    states = get_states(dataframe)
    collections = {}
    collections["UC3_WITHOUT_ANSWER"] = dataframe.loc[dataframe.usecase == 'remote_replacement'].uuid.unique()
    for state in states:
        collections[state[1]] = extract_collection(dataframe, state[0])

    # Troubleshooting Collection
    collections["UC3_Troubleshooting"] = np.setdiff1d(collections['UC3_YES'],collections['UC3_PRICE_PROMPT'])
    collections["UC3_SLOT_BOOKED"] = dataframe.loc[dataframe.remote_status == 'SUCCESS'].uuid.unique()
    return collections

def update_status(dataframe):
    collections = create_collections(dataframe)
    last_state = ['AGENT', 'IVR', 'HANGUP']
    for key, value in collections.items():
        dataframe['status'] = np.where(dataframe.uuid.isin(value), dataframe['language'] + '_' + key, dataframe['status'])

    for state in last_state:
        dataframe['status'] = np.where(dataframe.laststate.str.contains(state), dataframe['status'] + '_' + state, dataframe['status'])
    return dataframe

def update_column_name(dataframe):
    print(dataframe.head())
    dataframe.loc[0] = 'uuid', 'subscriber_id', 'caller_number', 'duration', 'state', 'cronological order', 'serviced status', 'status', 'start time', 'end time', 'language', 'laststate', 'usecase', 'new status', 'remote_status'

def uc3_breakdown_sheet(dataframe):
    preprocess = dataframe[dataframe["cronological order"]==0]
    breakdown = preprocess.groupby(['status']).size().reset_index(name='counts')
    collections = get_uc3_collections()
    contained_calls = get_uc3_contained_calls(preprocess, collections)
    uc3_update_serviced_status(dataframe, collections)
    body, contained = get_email_body(breakdown, collections, contained_calls)
    return breakdown, body, contained

def uc3_update_serviced_status(dataframe,collections):
    dataframe.loc[dataframe.status.isin(collections.get('Troubleshooting successful')) | dataframe.status.isin(collections.get('Work order created')), 'serviced status'] = 'Yes'

def get_uc3_contained_calls(dataframe,collections):
    contained_calls = dataframe.loc[dataframe.status.isin(collections.get('Troubleshooting successful'))].subscriber_id.tolist()
    contained_calls += dataframe.loc[dataframe.status.isin(collections.get('Work order created'))].subscriber_id.tolist()
    transfered_subs = dataframe.loc[(dataframe.status.str.contains('AGENT',case=False) | dataframe.status.str.contains('IVR',case=False)) & (dataframe.subscriber_id.isin(contained_calls) & dataframe.usecase.isin(['remote_replacement']))].subscriber_id.tolist()
    for sub in contained_calls:
        if sub in transfered_subs:
            contained_calls.remove(sub)
    return len(contained_calls)


def uc3_reports_raw_count(dataframe,collection):
    return dataframe.loc[dataframe.status.isin(collection)].counts.sum()


def get_email_body(dataframe, collections, contained_calls):
    total_calls = 0
    body = f""
    serviced_calls = 0
    message = f""
    breakdown = PrettyTable(['Status', 'Count'])
    for key, value in collections.items():
        count = uc3_reports_raw_count(dataframe,value)
        message += f"{key} : {count}\n"
        breakdown.add_row([key, count])
        total_calls += count
        if key == 'Troubleshooting successful' or key == 'Work order created':
            serviced_calls += count
    # body += f"Usecase 3 Breakdown: \nTotal Calls : {total_calls}\n"
    breakdown.add_row(['Total Calls', total_calls])

    contained = PrettyTable(['Status', 'Count', 'Percentage'])
    contained.add_row(['Contained Calls', contained_calls, "{:.2f}".format((contained_calls/total_calls)*100)])
    contained.add_row(['Serviced Calls', serviced_calls, "{:.2f}".format((serviced_calls/total_calls)*100)])

    # body = breakdown
    # print(breakdown)
    # body += f"Contained Calls : {contained_calls}\nContained Percentage : {round((contained_calls/total_calls)*100,2)}\n"
    # body += f"Total Serviced Calls : {serviced_calls}\nServiced Percentage : {round((serviced_calls/total_calls)*100,2)}\n"
    # body += f"\n\n{message}"
    return breakdown, contained

def get_uc3_collections():
    collections = {}
    troubleshooting_collection = ['SUCCESSFUL_SUBSEQUENT_QUERY', 'english_UC3_NO_HANGUP', 'hindi_UC3_NO_HANGUP','english_UC3_Troubleshooting_HANGUP','hindi_UC3_Troubleshooting_HANGUP','english_UC3_PRICE_PROMPT_HANGUP','hindi_UC3_PRICE_PROMPT_HANGUP','english_UC3_SLOT_PRESENTED_HANGUP','hindi_UC3_SLOT_PRESENTED_HANGUP']
    workorder_collection = ['english_UC3_SLOT_BOOKED_HANGUP','hindi_UC3_SLOT_BOOKED_HANGUP','english_UC3_SLOT_BOOKED','hindi_UC3_SLOT_BOOKED']
    agent_collection = ['hindi_UC3_WITHOUT_ANSWER_AGENT','english_UC3_SLOT_BOOKED_AGENT','hindi_UC3_SLOT_BOOKED_AGENT','english_UC3_SLOT_PRESENTED_AGENT','hindi_UC3_SLOT_PRESENTED_AGENT','english_UC3_PRICE_PROMPT_AGENT','hindi_UC3_PRICE_PROMPT_AGENT','english_UC3_WITHOUT_ANSWER_AGENT','english_UC3_Troubleshooting_AGENT','hindi_UC3_Troubleshooting_AGENT','english_UC3_NO_AGENT','hindi_UC3_NO_AGENT']
    ivr_collection = ['hindi_UC3_WITHOUT_ANSWER_IVR','english_UC3_SLOT_BOOKED_IVR','english_UC3_SLOT_PRESENTED_IVR','hindi_UC3_SLOT_PRESENTED_IVR','english_UC3_PRICE_PROMPT_IVR','hindi_UC3_PRICE_PROMPT_IVR','english_UC3_WITHOUT_ANSWER_IVR','english_UC3_Troubleshooting_IVR','hindi_UC3_Troubleshooting_IVR','english_UC3_NO_IVR','hindi_UC3_NO_IVR']
    disconnect_collection = ['english_UC3_SLOT_PRESENTED','hindi_UC3_SLOT_PRESENTED','english_UC3_PRICE_PROMPT','hindi_UC3_PRICE_PROMPT','english_UC3_WITHOUT_ANSWER','english_UC3_Troubleshooting','hindi_UC3_Troubleshooting','english_UC3_NO','hindi_UC3_NO', 'hindi_UC3_WITHOUT_ANSWER']
    collections['Troubleshooting successful'] = troubleshooting_collection
    collections['Work order created'] = workorder_collection
    collections['Agent transfer'] = agent_collection
    collections['IVR transfer'] = ivr_collection
    collections['Disconections'] = disconnect_collection
    return collections

if __name__ == '__main__':
    API_TOKEN = get_api_token()
    waterfall_data = get_export_file(QUESTION_ID)
    waterfall_df,fig = grenerate_report(waterfall_data)
    traversal_data = get_export_file(QUESTION_ID_TRAVERSAL) 
    traversal_df = traversal_report(traversal_data)
    #File Name
    time_now,name = get_time_and_name()
    AWS_FOLDER = create_aws_folder(time_now)
    traversal_df = update_status(traversal_df)
    uc3_breakdown, uc3_message, uc3_contained = uc3_breakdown_sheet(traversal_df)
    update_column_name(traversal_df)
    traversal_df.to_csv(f"{CSV_FOLDER}/{name}.csv",header=False,index=False)  
    traversal_df.to_csv(f"{AWS_FOLDER}/{name}.csv",header=False,index=False)
    try:
        update_sheet('Traversal',traversal_df.values.tolist(),'A:Z')
        update_sheet('Waterfall',waterfall_df.values.tolist(),'A:Z')
        update_sheet('UC3_Analysis',uc3_breakdown.values.tolist(),'A:Z')
        target = os.path.join(SHEET_FOLDER, f"{name}.xlsx")
        save_fig(fig,name)
        copy_to_folder(SHEET_ID,name)
        download_file(SHEET_ID,target)
        uploaded = upload_to_aws(f"{AWS_FOLDER}/{name}.csv", 'tatasky-sftp', f"{AWS_FOLDER}/{name}.csv")
        send_mail([[f"{name}.xlsx",SHEET_FOLDER],[f"{name}.png",IMAGE_FOLDER],[f"{name}.csv",CSV_FOLDER]],time_now, uc3_message, uc3_contained)
    except Exception as e:
        update_sheet('UC3_Analysis',uc3_breakdown.values.tolist(),'A:Z')
        target = os.path.join(SHEET_FOLDER, f"{name}.xlsx")
        save_fig(fig,name)
        copy_to_folder(SHEET_ID,name)
        download_file(SHEET_ID,target)
        uploaded = upload_to_aws(f"{AWS_FOLDER}/{name}.csv", 'tatasky-sftp', f"{AWS_FOLDER}/{name}.csv")
        send_mail([[f"{name}.xlsx",SHEET_FOLDER],[f"{name}.png",IMAGE_FOLDER],[f"{name}.csv",CSV_FOLDER]],time_now, uc3_message, uc3_contained)

